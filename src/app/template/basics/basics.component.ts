import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-basics',
  templateUrl: './basics.component.html',
  styles: [],
})
export class BasicsComponent {
  @ViewChild('myForm') myForm!: NgForm;

  readonly initialForm = {
    product: '',
    price: 0,
    stock: 0,
  };

  constructor() {}

  get isValidName(): boolean {
    return !(
      this.myForm?.controls.product?.invalid &&
      this.myForm?.controls.product?.touched
    );
  }

  get isValidPrice(): boolean {
    return !(
      this.myForm?.controls.price?.invalid &&
      this.myForm?.controls.price?.touched
    );
  }

  save() {
    console.log('form posteado', this.myForm.value);
    this.myForm.resetForm(this.initialForm);
  }
}
