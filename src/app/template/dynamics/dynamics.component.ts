import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

interface Person {
  name: string;
  favorites: Favorite[];
}

interface Favorite {
  id: number;
  name: string;
}

@Component({
  selector: 'app-dynamics',
  templateUrl: './dynamics.component.html',
  styles: [],
})
export class DynamicsComponent {
  @ViewChild('myForm') myForm!: NgForm;
  newGame: string = '';
  person: Person = {
    name: 'Jeanpier',
    favorites: [
      { id: 1, name: 'Warframe' },
      { id: 2, name: 'League of Legends' },
    ],
  };

  save() {
    console.log('form posteado', this.myForm.value);
  }

  delete(index: number) {
    this.person.favorites.splice(index, 1);
  }

  addGame() {
    if (!this.newGame.trim().length) return;
    const newFavorite: Favorite = {
      id: this.person.favorites.length + 1,
      name: this.newGame,
    };
    this.person.favorites.push({ ...newFavorite });
    this.newGame = '';
  }
}
