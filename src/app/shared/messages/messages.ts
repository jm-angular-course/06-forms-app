export const emailErrorMessages: { [key: string]: string } = {
  required: 'El email es obligatorio',
  pattern: 'El email debe ser válido',
  emailAlreadyExists: 'El email ya está registrado',
};

export const passwordErrorMessages: { [key: string]: string } = {
  required: 'El password es obligatorio',
  minlength: 'El password debe tener al menos 4 caracteres',
  doNotMatch: 'Las contraseñas no coinciden',
};

export const password2ErrorMessages: { [key: string]: string } = {
  doNotMatch: 'Las contraseñas no coinciden',
};

export const emailErrorMessagesMap: Map<string, string> = new Map([
  ['required', 'El email es obligatorio'],
  ['pattern', 'El email debe ser válido'],
  ['emailAlreadyExists', 'El email ya está registrado'],
]);
