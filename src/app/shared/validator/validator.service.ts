import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors } from '@angular/forms';

@Injectable({
  providedIn: 'root',
})
export class ValidatorService {
  emailPattern: string = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';
  fullnamePattern: string = '([a-zA-Z]+) ([a-zA-Z]+)';

  constructor() {}

  cantBeStrider(control: FormControl): ValidationErrors | null {
    const value = (control.value as string)?.trim().toLowerCase();
    if (value === 'strider' || value?.indexOf('strider') > -1) {
      return { cantBeStrider: true };
    }
    return null;
  }

  equalControls(control1: string, control2: string) {
    return function (formGroup: AbstractControl): ValidationErrors | null {
      const value1 = formGroup.get(control1)?.value;
      const value2 = formGroup.get(control2)?.value;
      if (value1 !== value2) {
        formGroup.get(control2)?.setErrors({ doNotMatch: true });
        return {
          doNotMatch: true,
        };
      }
      formGroup.get(control2)?.setErrors(null);
      return null;
    };
  }

  isEqual(value: string) {
    return (control: FormControl): ValidationErrors | null => {
      console.log(value);
      if ((control.value as string) === value) {
        return {
          doNotMatch: true,
        };
      }
      return null;
    };
  }
}
