import { FormControl } from '@angular/forms';

export const emailPattern: string = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';
export const fullnamePattern: string = '([a-zA-Z]+) ([a-zA-Z]+)';
export function cantBeStrider(control: FormControl) {
  const value = (control.value as string)?.trim().toLowerCase();
  if (value === 'strider' || value?.indexOf('strider') > -1) {
    return { cantBeStrider: true };
  }
  return null;
}
