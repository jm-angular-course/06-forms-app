import { Component } from '@angular/core';

class MenuItem {
  constructor(public text: string, public route: string) {}
}

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styles: [
    `
      li {
        cursor: pointer;
      }
    `,
  ],
})
export class SidemenuComponent {
  templateMenu: MenuItem[] = [
    new MenuItem('Básicos', '/template/basics'),
    new MenuItem('Dinámicos', '/template/dynamics'),
    new MenuItem('Switches', '/template/switches'),
  ];

  reactiveMenu: MenuItem[] = [
    new MenuItem('Básicos', '/reactive/basics'),
    new MenuItem('Dinámicos', '/reactive/dynamics'),
    new MenuItem('Switches', '/reactive/switches'),
  ];
  authMenu: MenuItem[] = [
    new MenuItem('Registro', '/auth/register'),
    new MenuItem('Login', '/auth/login'),
  ];
}
