import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  emailErrorMessages,
  password2ErrorMessages,
  passwordErrorMessages,
} from 'src/app/shared/messages/messages';
import { EmailValidatorService } from 'src/app/shared/validator/email-validator.service';
import { ValidatorService } from 'src/app/shared/validator/validator.service';

interface RegisterDto {
  fullname: string;
  email: string;
  username: string;
  password: string;
  password2: string;
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styles: [],
})
export class RegisterComponent implements OnInit {
  myForm: FormGroup = this.formBuilder.group(
    {
      fullname: [
        '',
        [
          Validators.required,
          Validators.pattern(this.validatorService.fullnamePattern),
        ],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern(this.validatorService.emailPattern),
        ],
        [this.emailValidator],
      ],
      username: [
        '',
        [Validators.required, this.validatorService.cantBeStrider],
      ],
      password: ['', [Validators.required, Validators.minLength(4)]],
      password2: ['', []],
    },
    {
      validators: [
        this.validatorService.equalControls('password', 'password2'),
      ],
    }
  );

  get emailError(): string {
    if (!this.myForm.get('email')?.touched) return '';
    const errorKey: string = this.getKeyErrorFromControl('email');

    return emailErrorMessages[errorKey];
  }

  get passwordError(): string {
    if (!this.myForm.get('password')?.touched) return '';
    const errorKey: string = this.getKeyErrorFromControl('password');

    return passwordErrorMessages[errorKey];
  }

  get password2Error(): string {
    if (!this.myForm.get('password2')?.touched) return '';
    const errorKey: string = this.getKeyErrorFromControl('password2');

    return password2ErrorMessages[errorKey];
  }

  constructor(
    private formBuilder: FormBuilder,
    private validatorService: ValidatorService,
    private emailValidator: EmailValidatorService
  ) {}

  ngOnInit(): void {
    this.myForm.reset({
      fullname: 'Jeanpier Mendoza',
      email: 'test10@test.com',
      username: 'jeanpierm',
      password: '1234',
      password2: '1234',
    });
  }

  controlInvalid(control: string) {
    return (
      this.myForm.get(control)?.invalid && this.myForm.get(control)?.touched
    );
  }

  createUser() {
    if (this.myForm.invalid) {
      return;
    }
    console.log(this.myForm.value);
    this.myForm.markAllAsTouched();
    this.myForm.reset();
  }

  private getKeyErrorFromControl(control: string): string {
    const errors = this.myForm.get(control)?.errors;
    return Object.keys(errors!)[0];
  }
}
