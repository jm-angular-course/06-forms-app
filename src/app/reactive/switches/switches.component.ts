import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-switches',
  templateUrl: './switches.component.html',
  styles: [],
})
export class SwitchesComponent implements OnInit {
  myForm: FormGroup = this.formBuilder.group({
    gender: ['M', Validators.required],
    notifications: [true, Validators.required],
    conditions: [false, Validators.requiredTrue],
  });

  person = {
    gender: 'F',
    notifications: true,
  };

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.myForm.reset(this.person);
    // this.myForm.get('conditions')?.valueChanges.subscribe((newValue) => {
    //   console.log(newValue);
    // });
    // this.myForm.valueChanges.subscribe(({ conditions, ...rest }) => {
    //   this.person = rest;
    // });
  }

  save() {
    if (this.myForm.invalid) {
      alert('Acepte los términos y condiciones de uso');
      return;
    }
    const { conditions, ...rest } = this.myForm.value;
    this.person = rest;
    console.log('Persona guardada', this.person);
  }
}
