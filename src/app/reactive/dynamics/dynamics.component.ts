import { Component } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-dinamics',
  templateUrl: './dynamics.component.html',
  styles: [],
})
export class DynamicsComponent {
  myForm: FormGroup = this.formBuilder.group({
    name: ['Warframe', [Validators.required, Validators.minLength(3)]],
    favorites: this.formBuilder.array(
      [
        ['Metal Gear', Validators.required],
        ['Death Stranding', Validators.required],
      ],
      Validators.required
    ),
  });

  newFavorite: FormControl = this.formBuilder.control('', Validators.required);

  get favoritesArray() {
    return this.myForm.get('favorites') as FormArray;
  }

  constructor(private formBuilder: FormBuilder) {}

  public save() {
    if (this.myForm.invalid) {
      return this.myForm.markAllAsTouched();
    }
    console.log(this.myForm.value);
    this.reset();
  }

  public reset() {
    this.myForm.reset();
  }

  public isFormControlValid(name: string) {
    return (
      this.myForm.controls[name].errors && this.myForm.controls[name].touched
    );
  }

  public addFavorite(): void {
    if (this.newFavorite.invalid) {
      return;
    }
    // const newFavorite = new FormControl(
    // this.newFavorite.value,
    // Validators.required
    // );
    const newFavorite = this.formBuilder.control(
      this.newFavorite.value,
      Validators.required
    );
    this.favoritesArray.push(newFavorite);
    this.newFavorite.reset();
  }

  public delete(index: number) {
    this.favoritesArray.removeAt(index);
  }
}
