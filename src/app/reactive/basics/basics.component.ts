import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-basics',
  templateUrl: './basics.component.html',
  styles: [],
})
export class BasicsComponent implements OnInit {
  // myForm: FormGroup = new FormGroup({
  //   name: new FormControl('RTX 4080ti'),
  //   price: new FormControl(1500),
  //   stock: new FormControl(10),
  // });
  myForm: FormGroup = this.formBuilder.group({
    name: [undefined, [Validators.required, Validators.minLength(3)]],
    price: [undefined, [Validators.required, Validators.min(0)]],
    stock: [undefined, [Validators.required, Validators.min(0)]],
  });

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.myForm.reset({
      name: 'RTX 4080ti',
      price: 1500,
    });
  }

  public save() {
    if (this.myForm.invalid) {
      return this.myForm.markAllAsTouched();
    }
    console.log(this.myForm.value);
    this.reset();
  }

  public reset() {
    this.myForm.reset();
  }

  public isFormControlInvalid(name: string) {
    return (
      this.myForm.controls[name].errors && this.myForm.controls[name].touched
    );
  }
}
